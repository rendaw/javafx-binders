package com.zarbosoft.javafxbinders;

import java.util.Optional;
import java.util.function.Function;

public interface Binder<T> extends HalfBinder<T> {
  void set(T v);

  /**
   * For both forward and backward mappings, return a new value, or Optional.empty() if it is not
   * possible to determine if the state of the value has changed.
   *
   * @param forward Map the binder's inner value to the outer type
   * @param back Map an outer value to the inner value type
   * @param <U>
   * @return
   */
  default <U> Binder<U> bimap(Function<T, Optional<U>> outward, Function<U, Optional<T>> inward) {
    return new BimapBinder<T, U>(this, outward, inward);
  }

  default <U> IndirectBinder<U> indirectBimap(
      Function<T, Optional<U>> outward, Function<U, Optional> inward) {
    return new IndirectBinder<U>(map(outward), inward);
  }
}
