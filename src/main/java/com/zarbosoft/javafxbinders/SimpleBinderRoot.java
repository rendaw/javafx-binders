package com.zarbosoft.javafxbinders;

public class SimpleBinderRoot implements BinderRoot {
  private final HalfBinder binder;
  private final Object key;

  public SimpleBinderRoot(HalfBinder binder, Object key) {
    this.binder = binder;
    this.key = key;
  }

  @Override
  public void destroy() {
    binder.removeRoot(key);
  }
}
