package com.zarbosoft.javafxbinders;

import com.zarbosoft.rendaw.common.Assertion;
import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.zarbosoft.rendaw.common.Common.unopt;

public interface HalfBinder<T> {
  /**
   * @param listener Immediately called with latest value
   * @return
   */
  BinderRoot addListener(Consumer<T> listener);

  void removeRoot(Object key);

  /**
   * @param function Return a new value, or Optional.empty() if it is not possible to determine if
   *     the state of the value has changed.
   * @param <U>
   * @return
   */
  default <U> HalfBinder<U> map(Function<T, Optional<U>> function) {
    return new MapBinder(this, function);
  }

  default <U> IndirectHalfBinder<U> indirectMap(Function<T, Optional> function) {
    return new IndirectHalfBinder<U>(this, function);
  }

  Optional<T> asOpt();

  default boolean isPresent() {
    return asOpt().isPresent();
  }

  default T get() {
    return unopt(asOpt());
  }

  default ObservableValue<T> bind(Supplier<T> defaul) {
    return new ObservableValue<T>() {
      List<InvalidationListener> listeners = new ArrayList<>();
      BinderRoot outer =
          HalfBinder.this.addListener(v -> listeners.forEach(l -> l.invalidated(this)));

      @Override
      public void addListener(ChangeListener<? super T> listener) {
        throw new Assertion();
      }

      @Override
      public void removeListener(ChangeListener<? super T> listener) {
        throw new Assertion();
      }

      @Override
      public T getValue() {
        return asOpt().orElseGet(defaul);
      }

      @Override
      public void addListener(InvalidationListener listener) {
        listeners.add(listener);
      }

      @Override
      public void removeListener(InvalidationListener listener) {
        listeners.remove(listener);
      }
    };
  }
}
