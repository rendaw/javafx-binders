package com.zarbosoft.javafxbinders;

import com.zarbosoft.automodel.lib.WeakList;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Consumer;

import static com.zarbosoft.rendaw.common.Common.opt;
import static com.zarbosoft.rendaw.common.Common.unopt;

public class ManualHalfBinder<T> implements HalfBinder<T> {
  Optional<T> value = Optional.empty();
  WeakList<Consumer<T>> listeners = new WeakList<>();

  public void set(T value) {
    this.value = opt(value);
    for (Consumer<T> listener : new ArrayList<>(listeners)) {
      listener.accept(value);
    }
  }

  public void clear() {
    this.value = Optional.empty();
  }

  @Override
  public BinderRoot addListener(Consumer<T> listener) {
    listeners.add(listener);
    if (value.isPresent()) listener.accept(unopt(value));
    return new SimpleBinderRoot(this, listener);
  }

  @Override
  public void removeRoot(Object key) {
    listeners.remove(key);
  }

  @Override
  public Optional<T> asOpt() {
    return value;
  }
}
