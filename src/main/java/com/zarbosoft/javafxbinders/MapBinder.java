package com.zarbosoft.javafxbinders;

import com.zarbosoft.automodel.lib.WeakList;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.zarbosoft.rendaw.common.Common.unopt;

public class MapBinder<T, U> implements HalfBinder<U>, Consumer<T> {
  private final Function<T, Optional<U>> forward;
  private final BinderRoot root; // GC root
  Optional<U> last = Optional.empty();
  WeakList<Consumer<U>> listeners = new WeakList<>();

  public MapBinder(HalfBinder parent, Function<T, Optional<U>> forward) {
    this.forward = forward;
    root = parent.addListener(this);
  }

  @Override
  public BinderRoot addListener(Consumer<U> listener) {
    listeners.add(listener);
    if (last.isPresent()) listener.accept(unopt(last));
    return new SimpleBinderRoot(this, listener);
  }

  @Override
  public void removeRoot(Object key) {
    listeners.remove(key);
  }

  @Override
  public Optional<U> asOpt() {
    return last;
  }

  @Override
  public void accept(T t) {
    Optional<U> v = forward.apply(t);
    if (!v.isPresent()) return;
    if (v.equals(last)) return;
    last = v;
    listeners.forEach(l -> l.accept(unopt(v)));
  }
}
