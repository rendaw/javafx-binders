package com.zarbosoft.javafxbinders;

import javafx.beans.InvalidationListener;
import javafx.collections.ObservableSet;

import java.util.Optional;
import java.util.function.Consumer;

import static com.zarbosoft.rendaw.common.Common.opt;

public class SetPropertyHalfBinder<T extends ObservableSet> implements HalfBinder<T> {
  final T property;

  public SetPropertyHalfBinder(T property) {
    this.property = property;
  }

  @Override
  public BinderRoot addListener(Consumer<T> listener) {
    final InvalidationListener inner =
        c -> {
          listener.accept(property);
        };
    property.addListener(inner);
    listener.accept(property);
    return new SimpleBinderRoot(this, inner);
  }

  @Override
  public void removeRoot(Object key) {
    property.removeListener((InvalidationListener) key);
  }

  @Override
  public Optional<T> asOpt() {
    return opt(property);
  }
}
