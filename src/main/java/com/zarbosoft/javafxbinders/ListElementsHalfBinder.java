package com.zarbosoft.javafxbinders;

import com.zarbosoft.automodel.lib.WeakList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.opt;
import static com.zarbosoft.rendaw.common.Common.unopt;

public class ListElementsHalfBinder<T> implements HalfBinder<List<T>> {
  private final List<HalfBinder<T>> list;
  WeakList<Consumer<List<T>>> listeners = new WeakList<>();
  List<T> at;
  final List<BinderRoot> rootElements;
  private boolean listenerSuppress = true;

  public ListElementsHalfBinder(List<HalfBinder<T>> list) {
    this.list = list;
    Consumer<T> listener =
        u0 -> {
          update();
        };
    listenerSuppress = true;
    rootElements = list.stream().map(t -> t.addListener(listener)).collect(Collectors.toList());
    listenerSuppress = false;
    update();
  }

  private void update() {
    if (listenerSuppress) return;
    at =
        list.stream()
            .map(b -> b.asOpt())
            .filter(v -> v.isPresent())
            .map(v -> unopt(v))
            .collect(Collectors.toList());
    new ArrayList<>(listeners).forEach(l -> l.accept(at));
  }

  @Override
  public BinderRoot addListener(Consumer<List<T>> listener) {
    listeners.add(listener);
    listener.accept(at);
    return new SimpleBinderRoot(this, listener);
  }

  @Override
  public void removeRoot(Object key) {
    listeners.remove(key);
  }

  @Override
  public Optional<List<T>> asOpt() {
    return opt(at);
  }
}
