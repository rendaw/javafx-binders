package com.zarbosoft.javafxbinders;

import com.zarbosoft.automodel.lib.WeakList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.zarbosoft.rendaw.common.Common.opt;

public class AnyHalfBinder implements HalfBinder<Boolean> {
  private final WeakList<Consumer<Boolean>> listeners = new WeakList<>();

  @SuppressWarnings("unused")
  private final List<BinderRoot> sourceRoots;

  private final List<HalfBinder<Boolean>> sources;
  private boolean v = false;
  private boolean suppress = false;

  public AnyHalfBinder(HalfBinder<Boolean>... sources) {
    this(Arrays.asList(sources));
  }

  public AnyHalfBinder(List<HalfBinder<Boolean>> sources) {
    this.sources = sources;
    suppress = true;
    sourceRoots =
        sources.stream().map(s -> s.addListener(v -> update())).collect(Collectors.toList());
    suppress = false;
    update();
  }

  private void update() {
    if (suppress) return;
    boolean last = v;
    v = sources.stream().anyMatch(s -> s.asOpt().orElse(false));
    if (v == last) return;
    new ArrayList<>(listeners).forEach(l -> l.accept(v));
  }

  @Override
  public BinderRoot addListener(Consumer<Boolean> listener) {
    this.listeners.add(listener);
    listener.accept(v);
    return new SimpleBinderRoot(this, listener);
  }

  @Override
  public void removeRoot(Object key) {
    this.listeners.remove(key);
  }

  @Override
  public Optional<Boolean> asOpt() {
    return opt(v);
  }
}
