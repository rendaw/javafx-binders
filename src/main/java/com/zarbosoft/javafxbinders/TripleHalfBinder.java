package com.zarbosoft.javafxbinders;

import com.zarbosoft.automodel.lib.WeakList;
import com.zarbosoft.rendaw.common.Triple;

import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Consumer;

import static com.zarbosoft.rendaw.common.Common.opt;
import static com.zarbosoft.rendaw.common.Common.unopt;

/**
 * Create a value binding whose value changes when both sub-value bindings are present and change.
 *
 * @param <X>
 * @param <Y>
 * @param <Z>
 */
public class TripleHalfBinder<X, Y, Z> implements HalfBinder<Triple<X, Y, Z>> {
  private Optional<Triple<X, Y, Z>> last = Optional.empty();

  private final class Value<T> {
    Optional<T> last = Optional.empty();
    Object sourceRoot; // GC root
  }

  private final Value<X> value1 = new Value<>();
  private final Value<Y> value2 = new Value<>();
  private final Value<Z> value3 = new Value<>();
  private WeakList<Consumer<Triple<X, Y, Z>>> listeners = new WeakList<>();

  public TripleHalfBinder(HalfBinder<X> source1, HalfBinder<Y> source2, HalfBinder<Z> source3) {
    setSource(value1, source1);
    setSource(value2, source2);
    setSource(value3, source3);
  }

  private void setSource(Value value, HalfBinder source) {
    value.sourceRoot =
        ((HalfBinder) source)
            .addListener(
                v -> {
                  accept1(value, v);
                });
  }

  private void accept1(Value value, Object newValue) {
    value.last = opt(newValue);
    if (!value1.last.isPresent() || !value2.last.isPresent() || !value3.last.isPresent()) return;
    last =
        Optional.of(
            new Triple<X, Y, Z>(unopt(value1.last), unopt(value2.last), unopt(value3.last)));
    for (Consumer<Triple<X, Y, Z>> c : new ArrayList<>(listeners)) c.accept(unopt(last));
  }

  @Override
  public BinderRoot addListener(Consumer<Triple<X, Y, Z>> listener) {
    listeners.add(listener);
    if (last.isPresent()) listener.accept(unopt(last));
    return new SimpleBinderRoot(this, listener);
  }

  @Override
  public void removeRoot(Object key) {
    listeners.remove(key);
  }

  public BinderRoot addListener(Triple.Consumer<X, Y, Z> listener) {
    return addListener(p -> listener.accept(p.first, p.second, p.third));
  }

  @Override
  public Optional<Triple<X, Y, Z>> asOpt() {
    return last;
  }

  public <U> HalfBinder<U> map(Triple.Function<Optional<U>, X, Y, Z> function) {
    return map(p -> function.accept(p.first, p.second, p.third));
  }
}
