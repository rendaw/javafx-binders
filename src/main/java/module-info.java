module com.zarbosoft.javafxbinders {
  exports com.zarbosoft.javafxbinders;
  requires com.zarbosoft.automodel.lib;
  requires javafx.base;
  requires com.zarbosoft.rendaw.common;
  requires javafx.controls;
}